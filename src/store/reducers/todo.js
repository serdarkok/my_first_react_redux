import { ADD_ITEM } from '../actionTypes';

const initialState = {
    todo: null,
}

const todo = (state = initialState, action) => {
    switch (action.type) {
        case ADD_ITEM: {
            const todo = action.payload;
            return {
                ...state,
                todo,
            }
        }
        default:
            return state;
    }
}

export default todo;