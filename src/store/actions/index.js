import { ADD_ITEM } from '../actionTypes';

export const addItem = (content) => {
    console.log(content);
    return {
        type: ADD_ITEM,
        payload: content,
    }
}