import React from 'react';
import AddTodo from './components/addTodo';
import ListTodo from './components/ListTodo';
import { Button } from 'element-react';
import './App.scss';
import 'element-theme-default';

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <AddTodo/>
        <ListTodo/>
        <Button
          type="primary"
          size="small"
        >
          Giriş
        </Button>
      </div>
    );
  }
}

export default App;
