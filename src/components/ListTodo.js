import React, { Component } from 'react'
import { connect } from 'react-redux';

const ListTodo = class extends Component {
    constructor() {
        super();
        this.state = {
            todo: null,
        }
    }
    render() {
        return (
            <div>
                { this.props.todo }
            </div>
        )
    }
}

const mapStatetoProps = state => {
    return state.todo;
}

export default connect(mapStatetoProps)(ListTodo);
