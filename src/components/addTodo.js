import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addItem } from '../store/actions';

const mapDispatchToProps = (dispatch) => {
    return {
        todoData : (todo) => dispatch(addItem(todo)),
    }
}

class AddTodo extends Component {
    constructor() {
        super();
        this.state = {
            todo: '',
        }
    };
    updateInput(todo) {
        this.setState({todo});
    }
    
    handleInput(e) {
        console.log(this.state.todo);
        this.props.todoData(this.state.todo);
        // this.props.addItem(this.state.todo);
    };

    render() {
        return (
            <div style={{ marginTop: '20px'}}>
                <input type="text" onChange={e => this.updateInput(e.target.value)} value={this.state.todo} />
                <button onClick={this.handleInput.bind(this)}>Ekle</button>
            </div>
        )
    }
}

export default connect(null, mapDispatchToProps)(AddTodo);